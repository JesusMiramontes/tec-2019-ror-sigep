module PermissionsConcern
  extend ActiveSupport::Concern

  def is_normal_user?
    self.access_lvl >= 1
  end

  def is_editor?
    self.access_lvl >= 2
  end

  def is_admin?
    self.access_lvl >= 3
  end
end