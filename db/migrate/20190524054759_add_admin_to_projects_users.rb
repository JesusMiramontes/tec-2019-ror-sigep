class AddAdminToProjectsUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :projects_users, :admin, :boolean
  end
end
