class AddAccessLvlToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :access_lvl, :integer, default: 1
  end
end
